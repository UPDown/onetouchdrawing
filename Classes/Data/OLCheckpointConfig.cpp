//
//  OLCheckpointConfig.cpp
//  MyCppGame
//
//  Created by helfy on 14-4-28.
//
//

#include "OLCheckpointConfig.h"
#include "OLGlobalDefine.h"

OLCheckpointConfig::OLCheckpointConfig(void)
{
    
}
OLCheckpointConfig::~OLCheckpointConfig(void)
{
    
}


OLCheckpointConfig * OLCheckpointConfig::create(void)
{
	OLCheckpointConfig * ret = new OLCheckpointConfig();
    if (ret && ret->init())
    {
        ret->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }
	return ret;
}


bool OLCheckpointConfig::init()
{
    this->points = ccArrayNew(8);

    this->creatTest();
    return true;
}


void OLCheckpointConfig::creatTest()
{
    //3个点
    OLPoint *point1 = OLPoint::create();
    point1->pointIndex=0;
    ccArrayAppendObjectWithResize(this->points, point1);
    
    OLPoint *point2 = OLPoint::create();
    point2->pointIndex=1;
    ccArrayAppendObjectWithResize(this->points, point2);
    
    OLPoint *point3 = OLPoint::create();
    point3->pointIndex=2;
    ccArrayAppendObjectWithResize(this->points, point3);
    
    
    OLPoint *point4 = OLPoint::create();
    point4->pointIndex=3;
    ccArrayAppendObjectWithResize(this->points, point4);
    
    OLPoint *point5 = OLPoint::create();
    point5->pointIndex=4;
    ccArrayAppendObjectWithResize(this->points, point5);
    
    
    point1->addPointRelationshipPoint(point3);
    point1->addPointRelationshipPoint(point4);
    
    point2->addPointRelationshipPoint(point4);
    point2->addPointRelationshipPoint(point5);
    
//    point3->addPointRelationshipPoint(point5);
    
    point1->point = Point(200/2, 200/2);
    point2->point = Point(400/2, 200/2);
    point3->point = Point(500/2, 400/2);
    point4->point = Point(300/2, 500/2);
    point5->point = Point(100/2, 400/2);
    
}



