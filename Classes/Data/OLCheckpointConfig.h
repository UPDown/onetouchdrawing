//
//  OLCheckpointConfig.h
//  MyCppGame
//
//  Created by helfy on 14-4-28.
//
//

#ifndef __MyCppGame__OLCheckpointConfig__
#define __MyCppGame__OLCheckpointConfig__

#include <iostream>
#include "cocos2d.h"
#include "OLPoint.h"
//关卡配置
class OLCheckpointConfig :public cocos2d::Ref
{
public:
    
    OLCheckpointConfig();
    virtual ~OLCheckpointConfig();
    
    virtual bool init();
    
    
    static OLCheckpointConfig* create(void);
    
    
    cocos2d::ccArray *points; // 关卡点 存放OLPoint
    
    int checkPointIndex;  //第几关
private:
   void creatTest();
    
};

#endif /* defined(__MyCppGame__OLCheckpointConfig__) */
