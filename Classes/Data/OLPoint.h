//
//  OLPoint.h
//  MyCppGame
//
//  Created by helfy on 14-4-29.
//
//

#ifndef __MyCppGame__OLPoint__
#define __MyCppGame__OLPoint__

#include <iostream>
#include "cocos2d.h"
class OLPoint:public cocos2d::Ref
{
public:
    
    OLPoint();
    virtual ~OLPoint();
    
    virtual bool init();
    
    static OLPoint* create(void);
    

    bool isSelected;  //被选中  主要用户点高亮
    
  
    cocos2d::Point point;
    int pointIndex;
    
    cocos2d::ccArray * relationshipPoint;
    void reSet();
    
    void addPointRelationshipPoint(OLPoint *point);
    void removeAllRelationship();
    
    bool canSelectPointFrom(OLPoint *point);
    void addPointRelationshipSelectFrom(OLPoint *point);
    
    bool checkAllSelect();
    
    
    //FOR AI
    OLPoint *getAnyPoint();
    OLPoint *getNextPoint();
    
    int subPointIndexForAI;
};
#endif /* defined(__MyCppGame__OLPoint__) */
