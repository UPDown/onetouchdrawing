//
//  OLAI.cpp
//  OneTouchDrawing
//
//  Created by helfy on 14-5-4.
//
//

#include "OLAI.h"
#include "OLGlobalDefine.h"

bool OLAI::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    checkPointArray = ccArrayNew(0);
    this->doNextStep = nullptr;
    this->doPreStep = nullptr;
    this->currentPoint = nullptr;
    currentSubIndex = 0;
    startIndex =0;
    return true;
}


void OLAI::startAI(cocos2d::ccArray *array)
{
    ccArrayAppendArrayWithResize(checkPointArray, array);
//    currentPoint = checkPointArray->arr[0];
//    if(currentPoint currentPoint)
     this->currentPoint = (OLPoint *)checkPointArray->arr[startIndex];
//    schedule(schedule_selector(OLAI::doStep), 1);
//        scheduleOnce(schedule_selector(OLAI::doStep), 1);
}

void OLAI::doStep(float tt)
{
    OLPoint *nextPoint =currentPoint->getAnyPoint();
    
    if(nextPoint == nullptr)
    {//当前为空。下级都被选中过，返回上级
//        this->unschedule(schedule_selector(OLAI::doStep));
        currentPoint = (OLPoint *)this->doPreStep();
        
    }
    else{
    //判断结束
        
    }
    this->currentPoint=nextPoint;
    
    
    this->doNextStep(this->currentPoint);
}

