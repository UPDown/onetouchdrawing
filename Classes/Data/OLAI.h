//
//  OLAI.h
//  OneTouchDrawing
//
//  Created by helfy on 14-5-4.
//
//

#ifndef __OneTouchDrawing__OLAI__
#define __OneTouchDrawing__OLAI__

#include <iostream>
#include "cocos2d.h"
#include "OLPoint.h"
class OLAI:public cocos2d::Layer
{
public:
  
    
    virtual bool init();

    cocos2d::ccArray *checkPointArray;
    
    void startAI(cocos2d::ccArray *array);
  
    
    std::function<void(OLPoint *)> doNextStep;  //下一步回调
    std::function<OLPoint *()> doPreStep;  //返回上个point的
    std::function<bool()> isWIn;  //检查是否成功
    
    std::function<void(bool)> AICheckEnd;  //ai检测完成 返回 true 可以通过， false 错误配置
    CREATE_FUNC(OLAI);
    
private:
    void doStep(float tt);
    int startIndex; //从0个点作为开始点，尝试是否可以畅通 ，记录起始点的序号
    OLPoint *currentPoint;
    int currentSubIndex;
};
#endif /* defined(__OneTouchDrawing__OLAI__) */
