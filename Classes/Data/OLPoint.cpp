//
//  OLPoint.cpp
//  MyCppGame
//
//  Created by helfy on 14-4-29.
//
//

#include "OLPoint.h"
#include "OLGlobalDefine.h"

OLPoint::OLPoint(void)
{

}
OLPoint::~OLPoint(void)
{
    
}


OLPoint * OLPoint::create(void)
{
	OLPoint * ret = new OLPoint();
    if (ret && ret->init())
    {
        ret->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }
	return ret;
}


bool OLPoint::init()
{
    
    relationshipPoint = ccArrayNew(0);

    return true;
}

void OLPoint::addPointRelationshipPoint(OLPoint * point)
{
    
    bool contains = false;

    ccArray *relationshipPoint =  point->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
        if(newPoint == this)  //防止重复
        {
            contains = true;
        }
    }
    
    if(contains == false)
    {
        __Dictionary *dic = __Dictionary::create();
        dic->setObject(point, "point");
        Ref *boolValue =__Bool::create(false);
        dic->setObject(boolValue, "isSelect");
        
        ccArrayAppendObjectWithResize(this->relationshipPoint, dic);
        
        
        dic = __Dictionary::create();
        dic->setObject(this, "point");
        boolValue =__Bool::create(false);
        dic->setObject(boolValue, "isSelect");
        ccArrayAppendObjectWithResize(point->relationshipPoint, dic);
    }
    
//    relationshipPoint =  this->relationshipPoint;
//    for (int k=0; k<relationshipPoint->num; k++) {
//        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
//        OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
//        if(newPoint == point)  //防止重复
//        {
//            Ref *boolValue =__Bool::create(true);
//            dic->setObject(boolValue, "isSelect");
//        }
//    }
//    
   
  
}
void OLPoint::removeAllRelationship()
{
    ccArrayRemoveAllObjects(relationshipPoint);
}

bool OLPoint::canSelectPointFrom(OLPoint *point)
{
    if(point == nullptr)return true;
    if(point == this)return false;
    
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
        if(newPoint == point)  //防止重复
        {
               __Bool *boolValue =(__Bool *) dic->objectForKey("isSelect");

            return !boolValue->getValue();
        }
    }
    
    return false;
}

void OLPoint:: addPointRelationshipSelectFrom(OLPoint *point)
{
    //
    if(point == nullptr)
    {
        return;
    }
    
    
    ccArray *relationshipPoint =  point->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
         __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
        if(newPoint == this)  //防止重复
        {
            Ref *boolValue =__Bool::create(true);
            dic->setObject(boolValue, "isSelect");
        }
    }
    
    
    relationshipPoint =  this->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
        if(newPoint == point)  //防止重复
        {
            Ref *boolValue =__Bool::create(true);
            dic->setObject(boolValue, "isSelect");
        }
    }
    
    
}

void OLPoint::reSet()
{
    this->isSelected = false;
    ccArray * relationshipPoint =  this->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        Ref *boolValue =__Bool::create(false);
        dic->setObject(boolValue, "isSelect");
    
    }
}

bool OLPoint::checkAllSelect()
{
    if (! this->isSelected) {
        return false;
    }
    
    ccArray *relationshipPoint =this->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        __Bool *boolValue =(__Bool *) dic->objectForKey("isSelect");
        if(!boolValue->getValue())
        {
            return false;
        }
    }
    
    return true;
}

OLPoint* OLPoint::getAnyPoint()
{
        ccArray *relationshipPoint =this->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        __Bool *boolValue =(__Bool *) dic->objectForKey("isSelect");
        if(!boolValue->getValue())
        {
            OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
            return newPoint;
        }
    }
    
    return nullptr;
}

OLPoint* OLPoint::getNextPoint()
{
    ccArray *relationshipPoint =this->relationshipPoint;
    for (int k=0; k<relationshipPoint->num; k++) {
        __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
        __Bool *boolValue =(__Bool *) dic->objectForKey("isSelect");
        if(!boolValue->getValue())
        {
            OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
            return newPoint;
        }
    }
    
    return nullptr;
}
