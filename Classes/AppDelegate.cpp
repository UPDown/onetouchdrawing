#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "OLGameScene.h"
#include "OLMenuScene.h"
#include "cocos-ext.h"

#include <cocosbuilder/CocosBuilder.h>

using namespace cocos2d::extension;
using namespace cocosbuilder;
USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("My Game");
        director->setOpenGLView(glview);
    }


    

    
    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    auto designSize = Size(480, 320);
    auto resourceSize = Size(480, 320);
    auto screenSize =  glview->getFrameSize();
    std::vector<std::string> searchPaths;
    std::vector<std::string> resDirOrders;
    
    searchPaths.push_back("Published-iOS"); // Resources/Published-iOS

    auto fileUtils = FileUtils::getInstance();
    fileUtils->setSearchPaths(searchPaths);
    
    if (screenSize.height > 320)
    {
        resourceSize = Size(960, 640);
        resDirOrders.push_back("resources-iphonehd");
    }
    else
    {
        resDirOrders.push_back("resources-iphone");
    }
    
    fileUtils->setSearchResolutionsOrder(resDirOrders);
    
    director->setContentScaleFactor(resourceSize.height/designSize.height);
    glview->setDesignResolutionSize(designSize.width, designSize.height, ResolutionPolicy::NO_BORDER);
    
    // run
    director->runWithScene(OLMenuScene::createFromCCB());
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
