//
//  OLGameScene.h
//  MyCppGame
//
//  Created by helfy on 14-4-25.
//
//

#ifndef __MyCppGame__OLGameScene__
#define __MyCppGame__OLGameScene__

#include <iostream>
#include "cocos2d.h"
#include "OLCheckpointConfig.h"
//游戏场景
class OLGameScene : public cocos2d::Layer
{
public:
    virtual bool init();
   static cocos2d::Scene *createScene();
    CREATE_FUNC(OLGameScene);
    virtual void draw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated) override;
    
    cocos2d::Touch *currentTouch;
    cocos2d::Label* label;
    
    void backCallback(Ref* sender);
private:
   cocos2d::ccArray *drawPoints;
    OLCheckpointConfig *checkpointConfig;
protected:
      void onDraw(const kmMat4 &transform, bool transformUpdated);
};


#endif /* defined(__MyCppGame__OLGameScene__) */
