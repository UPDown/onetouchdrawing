//
//  OLMakeScene.cpp
//  OneTouchDrawing
//
//  Created by helfy on 14-5-15.
//
//

#include "OLMakeScene.h"
#include "OLGlobalDefine.h"
Scene* OLMakeScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = OLMakeScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}