//
//  OLMenuSceneLoader.h
//  OneTouchDrawing
//
//  Created by HDJ on 14-5-6.
//
//

#ifndef OneTouchDrawing_OLMenuSceneLoader_h
#define OneTouchDrawing_OLMenuSceneLoader_h

#include "OLMenuScene.h"

class CCBReader;

//游戏场景
class OLMenuSceneLoader : public cocosbuilder::LayerLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(OLMenuSceneLoader, loader);
protected:
    CCB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD(OLMenuScene);
};

#endif
