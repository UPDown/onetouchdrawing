//
//  OLGameScene.cpp
//  MyCppGame
//
//  Created by helfy on 14-4-25.
//
//

#include "OLGameScene.h"
#include "OLGlobalDefine.h"
#include "OLAI.h"

Scene* OLGameScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = OLGameScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


bool OLGameScene::init()
{
    if(!Layer::init())
    {
        return false;
    }
    drawPoints = ccArrayNew(0);
    checkpointConfig = OLCheckpointConfig::create();
    checkpointConfig->retain();  // 靠 不retain 居然points 就为null
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
 
    label = Label::createWithSystemFont("playing", "Arial", 24);
    label->setPosition(Point(origin.x + visibleSize.width/2,
                             origin.y + visibleSize.height - label->getContentSize().height));
    
    this->addChild(label, 1);
    //创建手势监听
    auto touchListerner = EventListenerTouchOneByOne::create();
    touchListerner->onTouchBegan = [this] (Touch *touch, Event* event){
        
                 label->setString("playing");

        Point startPoint =touch->getStartLocation();
    //TODO   直接从设定的几个点才能算
        for (int i=0; i<checkpointConfig->points->num; i++) {
            OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
            Point configPoint = point->point;
            
            OLPoint *firstPoint = nullptr;
      
           if(abs(startPoint.x - configPoint.x) <10 && abs(startPoint.y - configPoint.y) <10 && point->canSelectPointFrom(firstPoint))
            {
                ccArrayAppendObjectWithResize(drawPoints, point);
                point->isSelected = true;
                currentTouch =touch;
                
                // find first point
                point->addPointRelationshipSelectFrom(firstPoint);
                
                break;
            }
         
        }
        
        
    	return true;
    };
    touchListerner->onTouchMoved = [this] (Touch *touch, Event* event){
        //检查点
          Point locationPoint =touch->getLocation();
        for (int i=0; i<checkpointConfig->points->num; i++) {
            OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
            Point configPoint = point->point;
            
            OLPoint *firstPoint = nullptr;
            if (drawPoints->num >=1) {
                firstPoint = (OLPoint *)drawPoints->arr[drawPoints->num-1];
              
            }
            if(abs(locationPoint.x - configPoint.x) <10 && abs(locationPoint.y - configPoint.y) <10 && point->canSelectPointFrom(firstPoint))
            {
                ccArrayAppendObjectWithResize(drawPoints, point);
                point->isSelected = true;
                currentTouch =touch;
                
                // find first point
                point->addPointRelationshipSelectFrom(firstPoint);
                
                break;
            }
        }
        
    };
    touchListerner->onTouchEnded = [this] (Touch *touch, Event* event){
        // 结束
        //检查每个point是否都已经连接
        bool win = true;
        for (int i=0; i<checkpointConfig->points->num; i++) {
            OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
            win =point->checkAllSelect();
            if(win == false)
            {
                break;
            }
        }
        if(win)
        {
            label->setString("you win");
        }
        else{
            label->setString("you lost");
        }
        ccArrayRemoveAllObjects(drawPoints);
        for (int i=0; i<checkpointConfig->points->num; i++) {
            OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
            point->reSet();
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListerner, this);
    
    
    OLAI *ai = OLAI::create();

    this->addChild(ai);
    ai->startAI(checkpointConfig->points);
    
    ai->doNextStep = [this](OLPoint *point)
    {
     
        for (int i=0; i<checkpointConfig->points->num; i++) {
            OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
            Point configPoint = point->point;
            
            OLPoint *firstPoint = nullptr;
            if (drawPoints->num >=1) {
                firstPoint = (OLPoint *)drawPoints->arr[drawPoints->num-1];
                
            }
            if(point->canSelectPointFrom(firstPoint))
            {
                ccArrayAppendObjectWithResize(drawPoints, point);
                point->isSelected = true;
        
                // find first point
                point->addPointRelationshipSelectFrom(firstPoint);
                
                break;
            }
        }
    };
    
    
    auto item = MenuItemImage::create("CloseNormal.png", "CloseSelected.png", CC_CALLBACK_1(OLGameScene::backCallback, this) );
    auto menu = Menu::create(item, NULL);
    item->setPosition(200,20);
    addChild(menu, 1);
    
    return true;
}

void OLGameScene::backCallback(Ref* sender)
{

    Director::getInstance()->popScene();
}


void OLGameScene::draw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated)
{
    
    OLPoint *prePoint = nullptr;
    if (drawPoints->num >=1) {
        prePoint = (OLPoint *)drawPoints->arr[drawPoints->num-1];
    }
    
    glLineWidth(5.0f);//线条宽度
    DrawPrimitives::setDrawColor4B(200,200,200,200);//颜色
    for (int i=0; i<checkpointConfig->points->num; i++) {

        OLPoint *point = (OLPoint *)checkpointConfig->points->arr[i];
        Point startPoint = point->point;
             if(point->isSelected)
             {
                     DrawPrimitives::setDrawColor4B(200,0,0,200);//颜色
               
               DrawPrimitives::drawSolidCircle(startPoint, 10, 10, 10);
             }else{
                   DrawPrimitives::setDrawColor4B(200,200,200,200);//颜色
               DrawPrimitives::drawCircle(startPoint, 10, 10, 10, false);
             }
      
    
        ccArray *relationshipPoint =  point->relationshipPoint;
     
        for (int k=0; k<relationshipPoint->num; k++) {
            __Dictionary *dic = (__Dictionary *)relationshipPoint->arr[k];
            OLPoint *newPoint =(OLPoint *) dic->objectForKey("point");
            if(prePoint == newPoint || prePoint == point)
            {
                DrawPrimitives::setDrawColor4B(0,200,0,200);//颜色
            }
            else{
                DrawPrimitives::setDrawColor4B(200,200,200,200);//颜色
            }
            if(newPoint->pointIndex > point->pointIndex)  //防止重复
            {
                Point endPoint = newPoint->point;
            
                DrawPrimitives::drawLine(startPoint, endPoint);
            }
        }

    }
    
    DrawPrimitives::setDrawColor4B(255,0,0,255);//颜色

    for (int i=0; i<drawPoints->num; i++) {
        OLPoint *touch = (OLPoint *)drawPoints->arr[i];
   
        Point nextPoint = Point::ZERO;
        if(i+1<drawPoints->num)
        {
            OLPoint *nextOlPoint = (OLPoint *)drawPoints->arr[i+1];
            nextPoint=nextOlPoint->point;
                 DrawPrimitives::drawLine(touch->point, nextPoint);
        }
        else{
            if(currentTouch)
            {
            nextPoint = currentTouch->getLocation();
                DrawPrimitives::drawLine(touch->point, nextPoint);
            }
        }
       
        
        
   
    }

    
}