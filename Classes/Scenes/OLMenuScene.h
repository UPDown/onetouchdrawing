//
//  OLMenuScene.h
//  OneTouchDrawing
//
//  Created by HDJ on 14-5-5.
//
//

#ifndef __OneTouchDrawing__OLMenuScene__
#define __OneTouchDrawing__OLMenuScene__

#include <iostream>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"

//CCBAnimationManager.h>

//USING_NS_CC;
//class CCBReader;


using namespace cocos2d;
using namespace cocosbuilder;

//游戏场景
class OLMenuScene
: public cocos2d::Layer
, public CCBSelectorResolver
, public CCBMemberVariableAssigner
, public CCBAnimationManagerDelegate
{
public:
    
    OLMenuScene();
    virtual ~OLMenuScene();
    static Scene* createFromCCB();
    
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(OLMenuScene, create);
    
    virtual void onEnter();
    virtual void onExit();
    
    void setAnimationManager(CCBAnimationManager *pAnimationManager);
    virtual void completedAnimationSequenceNamed(const char *name);
    
    virtual cocos2d::SEL_MenuHandler onResolveCCBCCMenuItemSelector(cocos2d::Ref * pTarget, const char * pSelectorName);
    virtual cocos2d::extension::Control::Handler onResolveCCBCCControlSelector(cocos2d::Ref * pTarget, const char * pSelectorName);
    virtual cocos2d::SEL_CallFuncN onResolveCCBCCCallFuncSelector(Ref * pTarget, const char* pSelectorName);
    
    virtual bool onAssignCCBMemberVariable(cocos2d::Ref * pTarget, const char * pMemberVariableName, cocos2d::Node * node);
    
    virtual bool onAssignCCBCustomProperty(Ref* pTarget, const char* pMemberVariableName, const cocos2d::Value& pCCBValue);

    void startGame(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent);
    void settingAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent);

    void guoguanAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent);
    void wushuangAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent);
    void tianxiaAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent);
    
private:
    CCBAnimationManager *mAnimationManager;
    bool        isEnable;
};

//游戏场景
class OLMenuSceneLoader : public cocosbuilder::LayerLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(OLMenuSceneLoader, loader);
protected:
    CCB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD(OLMenuScene);
};

#endif /* defined(__OneTouchDrawing__OLMenuScene__) */
