//
//  OLMenuScene.cpp
//  OneTouchDrawing
//
//  Created by HDJ on 14-5-5.
//
//

#include "OLMenuScene.h"
#include "OLGameScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

OLMenuScene::OLMenuScene()
:mAnimationManager(NULL)
,isEnable(false)

{

}

OLMenuScene::~OLMenuScene()
{
    CC_SAFE_RELEASE_NULL(mAnimationManager);
}

void OLMenuScene::onEnter()
{
    Layer::onEnter();
}

void OLMenuScene::onExit()
{
    Layer::onExit();
}

Scene* OLMenuScene::createFromCCB()
{
    // create a scene. it's an autorelease object
    NodeLoaderLibrary* ccNodeLoaderLibrary = NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    ccNodeLoaderLibrary->registerNodeLoader("OLMenuScene",OLMenuSceneLoader::loader());
    
    // 创建一个 CCBReader
    cocosbuilder::CCBReader* ccbReader = new cocosbuilder::CCBReader(ccNodeLoaderLibrary);
    
    //Read a ccbi file
    auto menuScene = ccbReader->readNodeGraphFromFile("OLMenuScene.ccbi");
    ((OLMenuScene*)menuScene)->setAnimationManager(ccbReader->getAnimationManager());
    Scene *scene = CCScene::create();
    scene->addChild(menuScene);
    return scene;
}


void OLMenuScene::completedAnimationSequenceNamed(const char *name)
{
//    loadingViewDone = YES;
//    if (loadingViewDone && initialDone && getSysInfoDone && autoLoginDone && adDone) {
//        scheduleOnce(schedule_selector(TinLoadingScene::mainScene), 1.0);
//    }
    isEnable = true;
    CCLOG("AAA=completedAnimationSequenceNamed");
}

void OLMenuScene::setAnimationManager(CCBAnimationManager *pAnimationManager)
{
    CC_SAFE_RELEASE_NULL(mAnimationManager);
    mAnimationManager = pAnimationManager;
    mAnimationManager->setDelegate(this);
    CC_SAFE_RETAIN(mAnimationManager);
//    mAnimationManager->setDelegate(this);
    
//    mAnimationManager->setAnimationCompletedCallback(this, callfunc_selector(OLMenuScene::completedAnimation));
}


bool OLMenuScene::onAssignCCBMemberVariable(Ref * pTarget, const char * pMemberVariableName, Node * pNode) {
//    CCB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_guoguan", cocos2d::extension::ControlButton *, this->m_guoguan);
//    CCB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_wushuang", cocos2d::extension::ControlButton *, this->m_wushuang);
//    CCB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_tianxia", cocos2d::extension::ControlButton *, this->m_tianxia);
//    CCB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_label", cocos2d::Label *, this->m_label);
    return false;
}

bool OLMenuScene::onAssignCCBCustomProperty(Ref* pTarget, const char* pMemberVariableName, const Value& pCCBValue)
{
    bool bRet = false;
//    if (pTarget == this)
//    {
//        if (0 == strcmp(pMemberVariableName, "mCustomPropertyInt"))
//        {
//            this->mCustomPropertyInt = pCCBValue.asInt();
//            log("mCustomPropertyInt = %d", mCustomPropertyInt);
//            bRet = true;
//        }
//        else if ( 0 == strcmp(pMemberVariableName, "mCustomPropertyFloat"))
//        {
//            this->mCustomPropertyFloat = pCCBValue.asFloat();
//            log("mCustomPropertyFloat = %f", mCustomPropertyFloat);
//            bRet = true;
//        }
//        else if ( 0  == strcmp(pMemberVariableName, "mCustomPropertyBoolean"))
//        {
//            this->mCustomPropertyBoolean = pCCBValue.asBool();
//            log("mCustomPropertyBoolean = %d", mCustomPropertyBoolean);
//            bRet = true;
//        }
//        else if ( 0  == strcmp(pMemberVariableName, "mCustomPropertyString"))
//        {
//            this->mCustomPropertyString = pCCBValue.asString();
//            log("mCustomPropertyString = %s", mCustomPropertyString.c_str());
//            bRet = true;
//        }
//        
//    }
    
    
    return bRet;
}


void OLMenuScene::guoguanAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent)
{
    CCLOG("guoguanAction");
    if (this->isEnable) {
        auto scene = OLGameScene::createScene();
        TransitionCrossFade *transitionScene = TransitionCrossFade::create(1.2, scene);
        if (transitionScene) {
            Director::getInstance()->pushScene(transitionScene);
        }
    }
}

void OLMenuScene::wushuangAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent)
{
    CCLOG("wushuangAction");
    if (this->isEnable) {
    
        auto scene = OLGameScene::createScene();
        TransitionRotoZoom *transitionScene = TransitionRotoZoom::create(1.2, scene);
        if (transitionScene) {
            Director::getInstance()->pushScene(transitionScene);
        }
    }
}

void OLMenuScene::tianxiaAction(cocos2d::Ref * sender, cocos2d::extension::Control::EventType pControlEvent)
{
    CCLOG("tianxiaAction");
    if (this->isEnable) {
        auto scene = OLGameScene::createScene();
        TransitionJumpZoom *transitionScene = TransitionJumpZoom::create(1.2, scene);
        if (transitionScene) {
            Director::getInstance()->pushScene(transitionScene);
        }
    
    }
}

Control::Handler OLMenuScene::onResolveCCBCCControlSelector(Ref * pTarget, const char * pSelectorName) {
//    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "startGame", OLMenuScene::startGame);
//    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "setUp", OLMenuScene::settingAction);
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "guoguanAction", OLMenuScene::guoguanAction);
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "wushuangAction", OLMenuScene::wushuangAction);
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "tianxiaAction", OLMenuScene::tianxiaAction);
    return NULL;
}

SEL_CallFuncN OLMenuScene::onResolveCCBCCCallFuncSelector(Ref * pTarget, const char* pSelectorName)
{
    CCLOG("completedAnimationSequenceNamed");
    //    CCB_SELECTORRESOLVER_CALLFUNC_GLUE(this, "onCallback1", TimelineCallbackTestLayer::onCallback1);
    //    CCB_SELECTORRESOLVER_CALLFUNC_GLUE(this, "onCallback2", TimelineCallbackTestLayer::onCallback2);
    return NULL;
}

SEL_MenuHandler OLMenuScene::onResolveCCBCCMenuItemSelector(Ref * pTarget, const char * pSelectorName) {
    return NULL;
}

